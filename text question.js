// WebDevelopment II – Lab #1

// Objective: In this lab we will start to practice promises and fetch API .

// What are promises? What are the different states of a promise.
// promise help us work with async code. We usually wrap our async code in promise.

// What are the 2 parameters inside the callback functions that the promise returns?
// resolve, reject
// resolve = promise ended without an error
// reject = promise ended with an error

// How do you use promises? Give  an example.
// calling an api

// What is the difference between asynchronous and synchronous?
// asynchronous multiple task can run at once

// How do we catch errors for promises?
// .catch

// How do you write a try-catch block?
// try {

// } catch (error) {

// }

// Try to convert from .then() => async await
// Give me 2 examples in a project when you want to use promises
//  const fetchData = () => new Promise((resolve, reject) => {...})
//  const sendData = () => new Promise((resolve, reject) => {...})
//  try {
//    await fetchData();
//    await sendData()
//  } catch (error) {
//    console.error("an error occurred", error)
//  }

// OUTPUT QUESTIONS***************

// 1 ) What is the output of the following code
// const promise = new Promise((resolve, reject) => {
//   reject(Error("Some error occurred"));
// });
// promise.catch((error) => console.log(error.message));
// promise.catch((error) => console.log(error.message));
// Some error occurred
// Some error occurred

// 2 )
// const promise = new Promise((resolve, reject) => {
//   reject(Error("Some Error Occurred"));
// })
//   .catch((error) => console.log(error))
//   .then((error) => console.log(error));
//   Some Error Occurred

//  3)
// async function func() {
//   return 10;
// }
// console.log(func());
// Promise { 10 }

// 4)
// async function func() {
//   await 10;
// }
// console.log(func());
// Promise { <pending> }

// 5)
// function delay() {
//   return new Promise((resolve) => setTimeout(resolve, 2000));
// }

// async function delayedLog(item) {
//   await delay();
//   console.log(item);
// }

// async function processArray(array) {
//   array.forEach((item) => {
//     await delayedLog(item);
//   });
// }

// processArray([1, 2, 3, 4]);
// await is only valid in async function
// array.forEach(async (item) => {
//   await delayedLog(item);
// });
// will fix it
