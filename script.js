function search(e) {
  e.preventDefault();
  const query = e.target["1"].value;

  const possibleMovie = movies
    .filter((m) =>
      m.original_title.toLocaleLowerCase().includes(query.toLocaleLowerCase())
    )
    .map((m) => m.original_title);

  let html = "<ul>";
  for (movie of possibleMovie) {
    html += `<li>${movie} </li>`;
  }
  html += "</ul>";
  document.getElementById("result").innerHTML = html;
}
